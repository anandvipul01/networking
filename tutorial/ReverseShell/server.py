import socket
import sys


# Create a Socket [connect two computer]
def create_socket():
    try:
        global host
        global port
        global s

        host = ''
        port = 9999
        s = socket.socket()
    except socket.error as msg:
        print('Socket Creation Error ', str(msg))

# Binding the host and the port with the socket
def bind_socket():
    try:
        global host
        global port 
        global s

        print('Binding the port', str(port))

        s.bind((host,port))
        s.listen(5)

    except socket.error as msg:
        print('Socket Binding Error' + str(msg) + '\n' + 'Retrying....')
        bind_socket()

# Sending Commands to client
def send_command(conn):
    while True:
        cmd = input()
        if cmd == 'quit':
            conn.close()
            s.close()
            sys.exit()

        if len(str.encode(cmd)) > 0:
            conn.send(str.encode(cmd))
            client_response = str(conn.recv(1024),'utf-8')
            print(client_response, end = '')

# Establish Connection with the client a socket must be listenieng
def socket_accept():
    conn, address = s.accept()
    print('Connection has been Established IP: ' + str(address[0]) + '; Port' + str(address[1]))
    send_command(conn)
    conn.close()

def main():
    create_socket()
    bind_socket()
    socket_accept()

if __name__ == '__main__':
    main()